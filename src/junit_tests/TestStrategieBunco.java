package junit_tests;

import static org.junit.Assert.*;

import bunco.JeuBunco;
import bunco.JeuBuncoFabrique;
import framework.*;
import framework.DeIterator;
import framework.JoueurIterator;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.jar.JarOutputStream;

public class TestStrategieBunco {

    private IStrategie jeuStrategie;
    private Jeu jeu;
    private JeuFabrique jeuFabrique;

    @Before
    public void setUp() throws Exception {
        jeuFabrique = new JeuBuncoFabrique();
        jeu = jeuFabrique.creerJeu();
        jeuStrategie = jeu.getStrategieDeJeu();
    }

    @Test
    public void calculerScoreVraiBuncoTest() {
        DeIterator deIterator = jeu.getCollectionDes().createIterator();

        jeu.setNbTour(0);

        while(deIterator.hasNext()){
            deIterator.next().setValeur(1);
        }

        int calculateScore = jeuStrategie.calculerScoreTour(jeu);
        assertEquals(21, calculateScore);
    }

    @Test
    public void calculerScoreFauxBuncoTest() {
        DeIterator deIterator = jeu.getCollectionDes().createIterator();
        while(deIterator.hasNext()){
            deIterator.next().setValeur(5);
        }
        jeu.setNbTour(0);

        int calculateScore = jeuStrategie.calculerScoreTour(jeu);
        assertEquals(5, calculateScore);
    }

    @Test
    public void calculerScore1PointsTest() {
        DeIterator deIterator = jeu.getCollectionDes().createIterator();
        int i = 1;

        jeu.setNbTour(0);

        while(deIterator.hasNext()){
            deIterator.next().setValeur(i);
            i++;
        }

        int calculateScore = jeuStrategie.calculerScoreTour(jeu);
        assertEquals(1, calculateScore);
    }

    @Test
    public void calculerScore2PointsTest() {
        DeIterator deIterator = jeu.getCollectionDes().createIterator();
        int i = 1;

        jeu.setNbTour(0);

        while(deIterator.hasNext()){
            if(i < jeu.getNbDesParJoueur()) {
                deIterator.next().setValeur(1);
            } else {
                deIterator.next().setValeur(5);
            }
            i++;
        }

        int calculateScore = jeuStrategie.calculerScoreTour(jeu);
        assertEquals(2, calculateScore);
    }

    @Test
    public void calculerLeVainqueurTest() {
        JoueurIterator joueurIterator = jeu.getCollectionJoueurs().createIterator();
        int i = 1;
        Joueur gagnantAttendu = null;
        ArrayList<Joueur> listJoueurPerdant = new ArrayList<>();
        while(joueurIterator.hasNext()){
            if(i < jeu.getNbDesParJoueur()) {
                Joueur perdant = joueurIterator.next();
                perdant.ajouterAuScore(i);
                listJoueurPerdant.add(perdant);
            } else {
                gagnantAttendu = joueurIterator.next();
                gagnantAttendu.ajouterAuScore(10);
            }
            i++;
        }
        Joueur gagnantActuel = jeuStrategie.calculerLeVainqueur(jeu);
        assertEquals(10, gagnantActuel.getScore());
        assertEquals(gagnantAttendu, gagnantActuel);

        for(Joueur perdant : listJoueurPerdant){
            assertTrue(perdant.getScore() < gagnantActuel.getScore());
        }
    }

    @Test
    public void verifierPasseAuProchainJoueurSiVraiBunco() {
        assertFalse(jeuStrategie.verifierSiRelancePossible(21));
    }

    @Test
    public void verifierResteAuMemeJoueurSiFauxBunco() {
        assertTrue(jeuStrategie.verifierSiRelancePossible(5));
    }

    @Test
    public void verifierResteAuMemeProchainJoueurSi1Point() {
        assertTrue(jeuStrategie.verifierSiRelancePossible(1));
    }

    @Test
    public void verifierResteAuMemeProchainJoueurSi2Point() {
        assertTrue(jeuStrategie.verifierSiRelancePossible(2));
    }

    @Test
    public void verifierPasseAuProchainJoueurSi0Point() {
        assertFalse(jeuStrategie.verifierSiRelancePossible(0));
    }

}
