package junit_tests;

import bunco.JeuBunco;
import bunco.JeuBuncoFabrique;
import org.junit.Before;
import framework.Jeu;
import framework.JeuFabrique;
import org.junit.Test;

import static org.junit.Assert.*;



public class JeuTest {

    private static Jeu jeu;
    private static JeuFabrique jeuFabrique;

    @Before
    public void setUp(){
        jeuFabrique = new JeuBuncoFabrique();
        jeu = jeuFabrique.creerJeu();
    }

    @Test
    public void testMethodeTemplate() {
        assertTrue(jeu.getJeuFabrique() != null);
        assertTrue(jeu.getCollectionJoueurs() != null);
        assertTrue(jeu.getCollectionDes() != null);
        assertTrue(jeu.getStrategieDeJeu() != null);
    }



}