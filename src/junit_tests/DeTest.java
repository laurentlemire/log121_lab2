package junit_tests;

import org.junit.Before;
import org.junit.Test;
import framework.De;
import static org.junit.Assert.*;


public class DeTest {

    private De de1;
    private De de2;

    @Before
    public void setUp() throws Exception {
        de1 = new De(6);
        de2 = new De(6);
    }

    @Test
    public void roulerDeTest() {
        int nombreDeFaces = de1.roulerDe();
        assertTrue(1 <= de1.getChiffrePresent() && de1.getChiffrePresent() <= nombreDeFaces);
    }

    @Test
    public void compareToDeSuperieurTest() {
        de1.setValeur(5);
        de2.setValeur(3);
        assertTrue(de1.compareTo(de2)==1);
    }

    @Test
    public void compareToDeInferieureTest() {
        de1.setValeur(5);
        de2.setValeur(3);
        assertTrue(de2.compareTo(de1)==-1);
    }

    @Test
    public void compareToMemeDeTest() {
        de1.setValeur(4);
        assertTrue(de1.compareTo(de1)==0);
    }

    @Test(expected=IllegalArgumentException.class)
    public void compareToNullDeTest(){
        de1.setValeur(4);
        de1.compareTo(null);
    }
}