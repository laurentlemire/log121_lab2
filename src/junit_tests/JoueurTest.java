package junit_tests;

import org.junit.Before;
import org.junit.Test;
import framework.Joueur;
import static org.junit.Assert.*;

public class JoueurTest {

    private Joueur joueur1;
    private Joueur joueur2;

    @Before
    public void setUp() throws Exception {
        joueur1 = new Joueur("Lemire");
        joueur2 = new Joueur("Cherif");
    }

    @Test
    public void compareToJoueurScoreSuperieurTest() {
        joueur1.ajouterAuScore(15);
        joueur2.ajouterAuScore(1);
        assertTrue(joueur1.compareTo(joueur2) == 1);
    }

    @Test
    public void compareToJoueurScoreInferieurTest() {
        joueur1.ajouterAuScore(1);
        joueur2.ajouterAuScore(10);
        assertTrue(joueur1.compareTo(joueur2) == -1);
    }

    @Test
    public void compareToJoueurScoreEgalTest() {
        joueur1.ajouterAuScore(10);
        joueur2.ajouterAuScore(10);
        assertTrue(joueur1.compareTo(joueur2) == 0);
    }

    @Test
    public void obtenirScoreAucunIncrementationTest() {
        assertTrue(joueur1.getScore() == 0);
    }

    @Test
    public void obtenirScoreApresIncrementationTest() {
        joueur1.ajouterAuScore(1);
        assertTrue(joueur1.getScore() == 1);
    }

    @Test
    public void obtenirScoreApresCumulIncrementationTest() {
        joueur1.ajouterAuScore(1);
        joueur1.ajouterAuScore(5);
        joueur1.ajouterAuScore(21);
        assertTrue(joueur1.getScore() == 1 + 5 + 21);
    }
}