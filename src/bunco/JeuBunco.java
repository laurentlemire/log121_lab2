package bunco;

import framework.*;
import framework.ArrayIterator;

/**
 * Sous-classe de Jeu décrivant le jeu Bunco
 */
public class JeuBunco extends Jeu {

    // Le nombre de dés totoal, le nombre de face par dé et le nombre de joueur
    private final int NB_DES = 3;
    private final int NB_FACES = 6;
    private final int NB_JOUEURS = 3;

    // String utilisé pour afficher le début d'une nouvelle partie
    private final String TITREJEU = "            Début du jeu\n-----------------------------------\n";

    /*
     ***********************************************************************************************
     * Méthodes utilisées pour créer l'instance de Jeu
     */

    @Override
    protected void initialiserFabrique() {
        super.setJeuFabrique(new JeuBuncoFabrique());
    }

    /**
     * Créé la collection de joueur du jeu
     */
    protected void CreerJoueurs() {
        super.setCollectionJoueurs(new CollectionJoueur());
        for (int i = 0; i < NB_JOUEURS; i++) {
            Joueur joueur = super.getJeuFabrique().creerJoueur("Joueur " + (i + 1));
            super.getCollectionJoueurs().addItem(joueur);
        }
    }

    /**
     * Créer la collection de dé du jeu
     */
    protected void CreerDes() {
        super.setCollectionDes(new CollectionDes());
        for (int i = 0; i < NB_DES; i++) {
            De de = super.getJeuFabrique().creerDe(NB_FACES);
            super.getCollectionDes().addItem(de);
        }
    }

    /**
     * Permet de choisir la stratégie utilisée lors du calcul de point d'un tour ainsi que pour choisir le gagnant.
     */
    protected void setupStrategie() {
        super.setStrategieDeJeu(super.getJeuFabrique().creerStratagie());
    }


    /**
     * Constructeur de JeuBunoo
     */
    public JeuBunco() {
        super();
        setNbDesParJoueur(NB_DES);
    }

    /*
     ***********************************************************************************************
     * Méthodes utilisées au cours d'une partie
     */

    /**
     * Réimplémentation de la méthode CommencerPartie.
     */
    @Override
    public void CommencerPartie() {

        // Variable dans lequel le status du jeu est enregistré
        boolean jeuFini = false;

        // Score du lancé de dé actuel
        int scoreDuLancerDeDes;

        // Affichage du début du jeu
        System.out.println(TITREJEU);

        // Tant que Jeu n'est pas considéré comme fini, le jeu continuera
        while(!jeuFini) {
            // Affichage du nombre de tour actuel
            System.out.println("\n            ----------\n            | Tour " + (super.getNbTour() + 1) + " |\n            ----------\n");

            // Création de l'itérateur pour la collectino de joueur
            ArrayIterator iteratorJoueur = this.getCollectionJoueurs().createIterator();

            // Actions qui seront éxécuté pour chaque joueur
            while (iteratorJoueur.hasNext()){
                Joueur j = (Joueur) iteratorJoueur.next();

                System.out.println("            " + j.getNom() + "\n----------------------------------------\n");

                boolean peutRelancer = true;

                // Tant que les dés lancés ne font pas un Bunco ou 0 point, le joueur peut continuer a lancer les dés
                while (peutRelancer) {
                    // On roule les dés
                    super.roulerTousLesDes();
                    // On affiche ;es nouvelles valeurs
                    afficherLesDes();

                    // On calcul le score du lancé de dé et on l'affiche
                    scoreDuLancerDeDes = calculerScoreTour();
                    System.out.println("Score du lancé de Dé : " + scoreDuLancerDeDes);

                    // On ajoute le score au joueur.
                    j.ajouterAuScore(scoreDuLancerDeDes);

                    // On vérifie si le joueur peut relancer les dés
                    peutRelancer = super.verifierSiRelancePossible(scoreDuLancerDeDes);

                }

                // On indique que le tour du joueur est fini et qu'un autre joueur est sur le point de lancer les dés
                System.out.println("\nFin des lancés de " + j.getNom() + "\n");
            }

            // Après que chaque joueur a lancé les dés, On regarde si le nombre de tour est 6, si oui : la partie se
            // termine. Sinon, le partie continue et on ajoute 1 au compteur de tour.
            if(super.getNbTour() >= 5) {
                jeuFini = true;
            }

            super.addToNbTour();
        }

        // On appel la méthode super de CommencerPartie pour afficher les scores finaux et déclarer le gagnant
        super.CommencerPartie();
    }

    /**
     * Affiche la collection de dé dans la console
     */
    public void afficherLesDes() {

        String StringDe = " +-+     +-+     +-+ \n |";

        ArrayIterator iteratorDe = this.getCollectionDes().createIterator();

        while (iteratorDe.hasNext()){

            De de = (De) iteratorDe.next();
            int currentNumber = de.getChiffrePresent();

            StringDe += currentNumber + "|     |";
        }

        StringDe = StringDe.substring(0, StringDe.length() - 1);
        StringDe += "\n +-+     +-+     +-+";
        System.out.println(StringDe);
    }
}


