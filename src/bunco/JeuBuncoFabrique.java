package bunco;

import framework.*;

/**
 * Classe qui créé une nouvelle instance de JeuBunco avec les valeurs appopriées
 */
public class JeuBuncoFabrique extends JeuFabrique {

    public Joueur creerJoueur(String nom){
        return new Joueur(nom);
    }

    public De creerDe(int nbFace){
        return new De(nbFace);
    }

    public Jeu creerJeu(){
        return new JeuBunco();
    }

    @Override
    public IStrategie creerStratagie() {
        return new StrategieBunco();
    }
}
