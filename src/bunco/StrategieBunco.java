package bunco;

import framework.De;
import framework.IStrategie;
import framework.Jeu;
import framework.Joueur;
import framework.ArrayIterator;

/**
 * Implémentation de IStrategie utilisé pour jouer au jeu Bunco
 */
public class StrategieBunco implements IStrategie {

    /**
     * Cette méthode calcule le score du joueur en fonction des dés lancés
     * 1. Si bunco, le score est 21.
     * 2. Vérifier la même valeur faciale pour tous les dés, puis renvoyer le score non-bunco
     * 3. Vérifier la correspondance de dés individuelle avec le tour en cours, puis attribuer
     *    un point pour chaque match
     * @param jeu instance jeu
     * @return le score du joueur
     */
    @Override
    public int calculerScoreTour(Jeu jeu) {

        /*
         * Un vrai Bunco signifie avoir 3 dés ou les chiffres sont tous pareil et ce chiffre correspond
         * au numéro du tour
         *
         * Un faux Bunco signifie avoir 3 dés ou les chiffres sont tous pareil mais ce chiffre ne correspond pas
         * au numéro du tour
         */

        // On assume que la possibilité d'un vrai bunco ou d'un faux bunco est encore possible
        boolean vraiBunco = true;
        boolean fauxBunco = true;

        // la valeur du score du lancé de dé
        int score = 0;

        // variable dans lequel la valeur du dernier dé est entreposé
        int dernierDe = 0;

        // Numéro du tour actuel
        int tour = jeu.getNbTour() + 1;

        ArrayIterator iteratorDe = jeu.getCollectionDes().createIterator();

        // liste d'actions effectués pour chaque dé
        while (iteratorDe.hasNext()){

            De de = (De) iteratorDe.next();

            // On enregistre la valeur du dé
            int currentNumber = de.getChiffrePresent();

            // On vérifie si la valeur du dé est égal au numéro du tour
            if(currentNumber != tour) {
                // Si ils ne sont pas équivalent, alors la possibilité d'un vrai Bunco devient nulle
                vraiBunco = false;

                /*
                 * Il reste encore la possibilité qu'un faux bunco soit possible. donc il faut maitenant
                 * vérifier la valeur de tous les dés et s'assurer qu'ils sont tous équivalents
                 */
                if(dernierDe != 0) {
                    // Si deux dés ne sont pas équivalents, alors la possibilité d'un faux Bunco devient nulle
                    if(dernierDe != currentNumber) {
                        fauxBunco = false;
                    }
                } else {
                    dernierDe = currentNumber;
                }
            } else {
                // Si la valeur du dé correspond au numéro du tour, alors la possibilité d'un faux bunco devient nulle
                fauxBunco = false;

                // On ajoute 1 point
                score++;
            }
        }

        // On vérifie si il y a eu un vrai Bunco ou un faux bunco
        if(vraiBunco) {
            // Si il y a un vrai Bunco, on change le pointage à 21
            score = 21;
        } else if(fauxBunco) {
            // Si il y a un faux Bunco, on change le pointage à 5
            score = 5;
        }

        return score;
    }

    /**
     * Cette méthode Détermine la gagnant du jeu en regardant le joueur avec le plus de point
     * @param jeu instance jeu
     * @return le gagnant sous forme d'instance de Joueur
     */
    @Override
    public Joueur calculerLeVainqueur(Jeu jeu) {
        ArrayIterator joueurIterator = jeu.getCollectionJoueurs().createIterator();
        Joueur gagnant = null;

        while (joueurIterator.hasNext()) {
            Joueur joueur = (Joueur) joueurIterator.next();
            if (gagnant == null){
                gagnant = joueur;
            } else if(gagnant.getScore() < joueur.getScore()){
                gagnant = joueur;
            }
        }

        return gagnant;
    }

    /**
     * Permet de vérifier à partir d'un score d'un lancé de dé si le joueur a le droit de relancer les dés
     * une autre fois
     * @param score - Le score du lancer de dé
     * @return - Un boolean indiquant si le joueur peut relancer les dés.
     */
    public boolean verifierSiRelancePossible(int score){
        return !(score == 0 || score == 21);
    }
}
