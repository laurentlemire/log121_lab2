package framework;

/**
 * Classe décrivant le comportement qu'une classe considéré comme une stratégie de Jeu devrait suivre.
 */
public interface IStrategie {
    /**
     * Cette méthode calcule le score d'un lancé de dé
     * @param jeu instance jeu
     * @return le score du joueur
     */
    public int calculerScoreTour(Jeu jeu);


    /**
     * Détermine un gagnant
     * @param jeu - le jeu dont les joueurs proviennent
     * @return - le gagnant sous forme d'objet de type Joueur
     */
    public Joueur calculerLeVainqueur(Jeu jeu);

    /**
     * Permet de vérifier à partir d'un score d'un lancé de dé si le joueur a le droit de relancer les dés
     * une autre fois
     * @param score - Le score du lancer de dé
     * @return - Un boolean indiquant si le joueur peut relancer les dés.
     */
    public boolean verifierSiRelancePossible(int score);
}
