package framework;

/**
 * Classe représentant une collection de Dés
 */
public class CollectionDes implements Agregat<De> {

    // Array dans lequel les dés de la collection sont entreposés
    private De[] tableauDes;

    // Index représentant le nombre de dés présent dans l'instance de la collection
    private int indexDe = 0;

    // Nombre maximum de dé dans un instance de cette collection
    private final int maxDe = 3;

    /**
     * Constructeur
     */
    public CollectionDes() {
        tableauDes = new De[maxDe];
    }

    /**
     * Rajoute un Dé à la collection
     * Si le nombre de Dé maximal de dé a été atteint, le dé ne sera pas ajouté.
     * @param de - Le dé à ajouter
     */
    public void addItem(De de) {
        if(indexDe < maxDe) {
            tableauDes[indexDe] = de;
            indexDe++;
        } else {
            System.out.println("Il y a déja " + maxDe + " Des dans cette collection");
        }
    }

    /**
     * Créer un objet DeIterator implémentant ArrayIterator pour cette collection
     * @return - L'objet DeIterator
     */
    public DeIterator createIterator() {
        return new DeIterator(tableauDes);
    }
}
