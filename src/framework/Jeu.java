package framework;

/**
 * Classe abstraite contenant les méthodes de base ou abstraite pour la création d'un jeu
 */
public abstract class Jeu {

    // Chiffre indiquant le numéro du tour
    private int nbTour;

    // Collections des joueurs et des dés utilisé
    private CollectionJoueur collectionJoueurs;
    private CollectionDes collectionDes;

    // Le nombre de dés utilisé par chaque joueur
    private int nbDesParJoueur;

    // Score total de tous les joueurs dans la partie
    private int totalScoreJoueurs = 0;

    // La stratégie utilisée pour calculer le score d'un tour ainsi que pour calculer le gagnant d'une partie
    private IStrategie strategieDeJeu;

    // Classe utilisé pour créer les instances de De et Joueur
    private JeuFabrique jeuFabrique;

    /**
     * Constructeur. Appel la méthode template CreerPartie() pour instancier toutes les variables nécessaires
     */
    public Jeu() {
        CreerPartie();
    }


    /*
     ***********************************************************************************************
     * Méthodes utilisées pour créer l'instance de Jeu
     */

    public void CreerPartie() {
        this.initialiserFabrique();
        this.CreerJoueurs();
        this.CreerDes();
        this.setupStrategie();
    }

    /**
     * Initialise la classe utilisée pour créer les instances de De et Joueur
     */
    protected abstract void initialiserFabrique();

    /**
     * Créé la collection de joueur du jeu
     */
    protected abstract void CreerJoueurs();

    /**
     * Créer la collection de dé du jeu
     */
    protected abstract void CreerDes();

    /**
     * Permet de choisir la stratégie utilisée lors du calcul de point d'un tour ainsi que pour choisir le gagnant.
     */
    protected abstract void setupStrategie();


    /*
     ***********************************************************************************************
     * Méthodes utilisées au cours d'une partie
     */

    /**
     * Calcul le score obtenu par un lancement de Dé.
     * @return - Le score du lancement de dé
     */
    public int calculerScoreTour() {
        return strategieDeJeu.calculerScoreTour(this);
    }

    /**
     * Détermine le gagnant du jeu en retournant l'instance de Joueur correspondant au gagnant
     * @return - Le gagnant en tant qu'objet de type Joueur
     */
    public Joueur calculerLeVainqueur() {
        return strategieDeJeu.calculerLeVainqueur(this);
    }

    /**
     * Permet de vérifier à partir d'un score d'un lancé de dé si le joueur a le droit de relancer les dés
     * une autre fois
     * @param score - Le score du lancer de dé
     * @return - Un boolean indiquant si le joueur peut relancer les dés.
     */
    public boolean verifierSiRelancePossible(int score) {
        return strategieDeJeu.verifierSiRelancePossible(score);
    }

    /**
     * Commence la partie.
     * De base, cette méthode ne fait qu'afficher les scores. Il faut donc l'Override dans une sous-classe
     */
    public void CommencerPartie() {
        AfficherScores();
    }

    /**
     * Affiche les scores de chaque joueur ainsi que le gagnant du jeu
     */
    public void AfficherScores() {
        AfficherTableScore();
        AfficherLeGagnant();
    }

    /**
     * Affiche le score de chaque joueur dans un tableau
     */
    private void AfficherTableScore() {
        String sFormat = "| %-11s | %-8d | %-6d |";
        System.out.println("+-------------|----------|--------+");
        System.out.println("|   Joueurs   |  Scores  |  Tour  |");
        System.out.println("|-------------|----------|--------|");
        ArrayIterator joueurIterator = collectionJoueurs.createIterator();
        while (joueurIterator.hasNext()) {
            Joueur joueur = (Joueur) joueurIterator.next();
            if (!joueurIterator.hasNext())
                sFormat = "| %-11s | %-8d | %-6d |";
            System.out.format(sFormat, joueur.getNom(), joueur.getScore(), this.getNbTour());
            totalScoreJoueurs += joueur.getScore();
            System.out.println("\r");
            System.out.println("|-------------|----------|--------|");
        }
        System.out.format(sFormat, "Total", totalScoreJoueurs, this.getNbTour());
        System.out.println("\r");
        System.out.println("+-------------|----------|--------+");
    }

    /**
     * Affiche le gagnant du jeu
     */
    private void AfficherLeGagnant() {
        Joueur gagnant = calculerLeVainqueur();
        System.out.println("Le gagnant est : "  + gagnant.getNom() + " avec " + gagnant.getScore() + " points.");
    }

    /**
     * Appel la méthode roulerDe() pour chaque Dés compris dans la collection collectionDes
     */
    protected void roulerTousLesDes() {

        ArrayIterator iteratorDe = this.getCollectionDes().createIterator();

        while (iteratorDe.hasNext()){
            De de = (De) iteratorDe.next();
            de.roulerDe();
        }
    }

    /**
     * Obtient la Collection de joueur
     * @return - collectionJoueur
     */
    public CollectionJoueur getCollectionJoueurs() {
        return collectionJoueurs;
    }

    /**
     * Obtient la collection de dés
     * @return - collectionDes
     */
    public CollectionDes getCollectionDes() {
        return collectionDes;
    }

    /**
     * Obtient la Collection de joueur
     * @return - collectionJoueur
     */
    public void setCollectionJoueurs(CollectionJoueur collJ) {
        this.collectionJoueurs = collJ;
    }

    /**
     * Obtient la collection de dés
     * @return - collectionDes
     */
    public void setCollectionDes(CollectionDes collDes) {
        this.collectionDes = collDes;
    }

    /**
     * Obtient le nombre de dés par joueur
     * @return - nbDesParJoueur
     */
    public int getNbDesParJoueur() {
        return nbDesParJoueur;
    }

    /**
     * Modifie le nombre de dés utilisé par chaque joueur
     * @param nbDesParJoueur - la nouvelle valeur
     */
    public void setNbDesParJoueur(int nbDesParJoueur) {
        this.nbDesParJoueur = nbDesParJoueur;
    }

    /**
     * Obtient le numéro du tour présent
     * @return - nbTour
     */
    public int getNbTour() {
        return nbTour;
    }

    /**
     * Augmente le numéro du tour de 1
     */
    public void addToNbTour() {
        this.nbTour += 1;
    }

    /**
     * Change la stratégie utilisé pour calculer le score d'un lancé de dé ainsi que le gagnant.
     * @param strategieDeJeu
     */
    public void setStrategieDeJeu(IStrategie strategieDeJeu) {
        this.strategieDeJeu = strategieDeJeu;
    }

    /**
     * Obtiens la stratégie utilisée pour calculer les points et le gagnant
     * @return - strategieDeJeu
     */
    public IStrategie getStrategieDeJeu() {
        return strategieDeJeu;
    }

    /**
     * Change le numero du tour
     */
    public void setNbTour(int nombreTour) {
        nbTour = nombreTour;
    }

    /**
     * Obtiens l'objet JeuFabrique du jeu
     * @return - jeuFabrique
     */
    public JeuFabrique getJeuFabrique() {
        return jeuFabrique;
    }

    /**
     * Change l'objet utilisé pour créer les instances de De et Joueur
     * @param jeuFabrique - la nouvelle classe Fabrique
     */
    public void setJeuFabrique(JeuFabrique jeuFabrique) {
        this.jeuFabrique = jeuFabrique;
    }
}
