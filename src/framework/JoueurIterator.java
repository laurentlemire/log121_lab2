package framework;

/**
 * Classe implémentant ArrayIterator pour une collection de type joueur
 */
public class JoueurIterator implements ArrayIterator {

    // Objets itérés
    private Joueur[] arrayJoueur;

    // Index dans le tableau
    private int index = 0;

    /**
     * Constructeur
     * @param tabJoueur - Tableau qui sera itéré
     */
    public JoueurIterator(Joueur[] tabJoueur) {
        arrayJoueur = tabJoueur;
    }

    /**
     * Vérifie si l'itérateur a fait le tour de la collection
     * @return - un boolean décrivant si il existe un autre Joueur disponible
     */
    public boolean hasNext() {
        if(index >= arrayJoueur.length || arrayJoueur[index] == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Retourne le prochain objet de la collection
     * @return - L'objet demandé
     */
    public Joueur next() {
        Joueur item = arrayJoueur[index];
        index++;
        return item;
    }
}
