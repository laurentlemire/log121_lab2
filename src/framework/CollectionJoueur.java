package framework;

/**
 * Classe représentant une collection de Joueurs
 */
public class CollectionJoueur implements Agregat<Joueur> {

    // Array dans lequel les joueurs de la collection sont entreposés
    private Joueur[] tableauJoueur;

    // Index représentant le nombre de joueurs présent dans l'instance de la collection
    private int indexJoueur = 0;

    // Nombre maximum de joueur dans un instance de cette collection
    private final int maxJoueur = 3;

    /**
     * Constructeur
     */
    public CollectionJoueur() {
        tableauJoueur = new Joueur[maxJoueur];
    }

    /**
     * Rajoute un Joueur à la collection
     * Si le nombre de joueur maximal de joueur a été atteint, le joueur ne sera pas ajouté.
     * @param joueur - Le joueur à ajouter
     */
    public void addItem(Joueur joueur) {
        if(indexJoueur < maxJoueur) {
            tableauJoueur[indexJoueur] = joueur;
            indexJoueur++;
        } else {
            System.out.println("Il y a déja " + maxJoueur +" Joueurs dans cette collection");
        }
    }

    /**
     * Créer un objet JoueurIterator implémentant ArrayIterator pour cette collection
     * @return - L'objet JoueurIterator
     */
    public JoueurIterator createIterator() {
        return new JoueurIterator(tableauJoueur);
    }
}
