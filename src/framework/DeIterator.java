package framework;

/**
 * Classe implémentant ArrayIterator pour une collection de type De
 */
public class DeIterator implements ArrayIterator {

    // Objets itérés
    private De[] arrayDe;

    // Index dans le tableau
    private int index = 0;

    /**
     * Constructeur
     * @param tabDe - Tableau qui sera itéré
     */
    public DeIterator(De[] tabDe) {
        arrayDe = tabDe;
    }

    /**
     * Vérifie si l'itérateur a fait le tour de la collection
     * @return - un boolean décrivant si il existe un autre Dé disponible
     */
    public boolean hasNext() {
        if(index >= arrayDe.length || arrayDe[index] == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Retourne le prochain objet de la collection
     * @return - L'objet demandé
     */
    public De next() {
        De item = arrayDe[index];
        index++;
        return item;
    }

}
