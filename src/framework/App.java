package framework;

import bunco.JeuBunco;
import bunco.JeuBuncoFabrique;

/**
 * Class utilisé pour éxécuter la méthode main()
 */
public class App {

    private static Jeu jeu;
    private static JeuFabrique jeuFabrique;

    /**
     * méthode main dans lequel le JeuBunco est fabriqué et lancé.
     * @param args - Arguments additionnel donné au compilatuer
     */
    public static void main(String[] args) {
        jeuFabrique = new JeuBuncoFabrique();
        jeu = jeuFabrique.creerJeu();
        jeu.CommencerPartie();
    }
}
