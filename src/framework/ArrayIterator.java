package framework;

/**
 * Interface décrivant le comportement d'un itérateur utilisé pour passez au travers d'une collection de type Array
 */
public interface ArrayIterator {

    /**
     * Vérifie si l'itérateur a fait le tour de la collection
     * @return - un boolean décrivant si il existe un autre objet disponible
     */
    public boolean hasNext();

    /**
     * Retourne le prochain objet de la collection
     * @return - L'objet demandé
     */
    public Object next();
}
