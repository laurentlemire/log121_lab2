package framework;

/**
 * Classe abstraite décrivant ls squelette d'une classe qui peut créer une instance d'une sous-classe de jeu
 */
public abstract class JeuFabrique {

    public abstract Joueur creerJoueur(String nom);

    public abstract De creerDe(int nbFace);

    public abstract Jeu creerJeu();

    public abstract IStrategie creerStratagie();
}
