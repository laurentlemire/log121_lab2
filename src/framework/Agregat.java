package framework;

/**
 * Interface décrivant une collection d'Objet
 * @param <T> - Type de l'objet de la collection
 */
public interface Agregat<T> {

    /**
     * Ajoute un item à la collection
     * @param obj - L'objet a ajouté
     */
    public void addItem(T obj);

    /**
     * Créé un itérateur pour la Collection
     * @return - Un itéateur de type ArrayIterator
     */
    public ArrayIterator createIterator();
}
