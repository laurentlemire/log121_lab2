package framework;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Classe représentant un dé
 */
public class De implements Comparable<De> {

    // Le nombre de face du dé
    private int nbFace;

    // Le chiffre affiché par le dé
    private int chiffrePresent;

    /**
     * Constructeur
     * @param nbFace - Nombre de face du dé
     */
    public De(int nbFace) {
        this.nbFace = nbFace;
    }

    // Code from https://stackoverflow.com/questions/363681/how-do-i-generate-random-integers-within-a-specific-range-in-java

    /**
     * Simule le roulement d'un dé. Après avoir exécuté cette méthode, chiffrePresent sera changé pour un nombre
     * aléatoire entre et le nombre de face, inclusivement.
     * @return - La nouvelle valeur du dé
     */
    public int roulerDe() {
        chiffrePresent = ThreadLocalRandom.current().nextInt(1, nbFace);
        return chiffrePresent;
    }

    /**
     * Le chiffre affiché par le dé
     * @return - chiffrePresent
     */
    public int getChiffrePresent() {
        return chiffrePresent;
    }

    /**
     * Le nombre de face du dé
     * @return - nbFace
     */
    public int getNbFace() {
        return nbFace;
    }

    /**
     * Modifie le nombre de la valeur de face de dé
     * @param faceValeur - La nouvelle valeur
     */
    public void setValeur(int faceValeur) {
        chiffrePresent = faceValeur;
    }


    @Override
    public int compareTo(De de) {
        if(de == null){
            throw new IllegalArgumentException("Le dé est null");
        }

        return ((Integer) getChiffrePresent()).compareTo(de.getChiffrePresent());
    }
}
