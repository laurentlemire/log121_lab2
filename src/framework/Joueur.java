package framework;

/**
 * Classe représentant un joueur
 */
public class Joueur implements Comparable<Joueur> {

    // Le nom du joueur
    private String nom;

    // Le score du joueur
    private int score = 0;

    /**
     * Constructeur.
     * @param nom - Nom du joueur
     */
    public Joueur(String nom) {
        this.nom = nom;
    }

    /**
     * Obtient le score actuel du joueur
     * @return
     */
    public int getScore() {
        return score;
    }

    /**
     * rajoute une quantité de point au score actuel du joueur
     * @param score - la quantité de point à ajouter
     */
    public void ajouterAuScore(int score) {
        this.score += score;
    }

    /**
     * Obtient le nom du joueur
     * @return - nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet de changr le nom du joueur
     * @param nom - le nouveau nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Permet de changer le score du joueur
     * @param score - le nouveau score
     */
    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public int compareTo(Joueur o) {
        if(o == null){
            throw new IllegalArgumentException("Le joueur o est null");
        }

        return ((Integer)this.score).compareTo(o.score);
    }
}
